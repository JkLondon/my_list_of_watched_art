from django.db import models
from django.urls import reverse


class Type(models.Model):
    name = models.CharField(max_length=200,
                            help_text="Введите тип произведения (книга/фильм/аниме)",
                            verbose_name="Тип произведения",
                            choices=[('Аниме', 'Аниме'), ('Фильм', 'Фильм'), ('Книга', 'Книга')])

    def __str__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(max_length=200,
                            help_text="Введите жанр произведения",
                            verbose_name="Жанр произведения")

    def __str__(self):
        return self.name


class Language(models.Model):
    name = models.CharField(max_length=20,
                            help_text="Введите язык произведения",
                            verbose_name="Язык произведения")

    def __str__(self):
        return self.name


class Author(models.Model):
    first_name = models.CharField(max_length=100,
                                  help_text="Введите имя автора",
                                  verbose_name="Имя автора")
    last_name = models.CharField(max_length=100,
                                 help_text="Введите фамилию автора",
                                 verbose_name="Фамилия авторав")
    date_of_birth = models.DateField(help_text="Введите дату рождения",
                                     verbose_name="Дата рождения",
                                     null=True, blank=True)
    date_of_death = models.DateField(help_text="Введите дату смерти",
                                     verbose_name="Дата смерти",
                                     null=True, blank=True)

    def __str__(self):
        return self.last_name


class Status(models.Model):
    name = models.CharField(max_length=200,
                            help_text="Введите статус произведения (Смотрю/Просмотрено/Запланировано)",
                            verbose_name="Статус произведения",
                            choices=[('Смотрю', 'Смотрю'), ('Просмотрено', 'Просмотрено'), ('Запланировано', 'Запланировано')])

    def __str__(self):
        return self.name


class Artwork(models.Model):
    author = models.ManyToManyField('Author',
                                    help_text="Введите автора книги",
                                    verbose_name="автор книги")

    type = models.ForeignKey('Type', on_delete=models.CASCADE,
                             help_text="Выберите тип произведения",
                             verbose_name="Тип произведения")
    genre = models.ForeignKey('Genre', on_delete=models.CASCADE,
                              help_text="Выберите жанр произведения",
                              verbose_name="Жанр произведения", null=True)
    title = models.CharField(max_length=200,
                             help_text="Введите название произведения",
                             verbose_name="Название произведения")
    language = models.ForeignKey('Language',
                                 on_delete=models.CASCADE,
                                 help_text="Выберите язык произведения",
                                 verbose_name="Язык произведения", null=True)
    summary = models.TextField(max_length=1000,
                               help_text="Введите краткое описание произведения",
                               verbose_name="Аннотация произведения")
    status = models.ForeignKey('Status',
                               on_delete=models.CASCADE,
                               help_text="Выберите статус произведения",
                               verbose_name="Статус произведения", null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('artwork-detail', args=[str(self.id)])

    def display_author(self):
        return ', '.join([author.last_name for author in
                          self.author.all()])
    display_author.short_description = 'Авторы'
