from django.shortcuts import render
from django.http import HttpResponse
from .models import Author, Artwork, Status, Type, Genre, Language


def index(request):
    num_arts = Artwork.objects.all().count()
    num_authors = Author.objects.all().count()

    return render(request, 'index.html',
                  context={'num_arts' : num_arts,
                           'num_authors' : num_authors})
