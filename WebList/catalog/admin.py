from django.contrib import admin
from .models import Author, Artwork, Genre, Language, Status, Type


'''
admin.site.register(Author)
admin.site.register(Artwork)
'''
admin.site.register(Genre)
admin.site.register(Type)
admin.site.register(Status)
admin.site.register(Language)


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'first_name', 'date_of_birth', 'date_of_death')
    fields = ['first_name', 'last_name',
              ('date_of_birth', 'date_of_death')]


admin.site.register(Author, AuthorAdmin)


@admin.register(Artwork)
class ArtworkAdmin(admin.ModelAdmin):
    list_display = ('type', 'title', 'genre', 'language', 'display_author')
    list_filter = ('genre', 'author')

